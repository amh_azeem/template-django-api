# TERMINOLOGIES
Task queues are used as a mechanism to distribute work across threads or machines
Celery communicates via messages, usually using a broker to mediate between clients and workers. To initiate a task the client adds a message to the queue, the broker then delivers that message to a worker.
* Redis and RabbitMQ are message brokers. This means they handle the queue of "messages" between Django and Celery.
virtual environment is a tool that helps to keep dependencies required by different projects separate by creating isolated python virtual environments for them
Task queues are used as a mechanism to distribute work across threads or machines


## SYSTEM REQUIREMENTS:
- CENTOS 7.0
- NGINX
- POSTGRES

### Steps
```shell script
yum groupinstall development
yum install -y epel-release python3-devel python3-pip python3
```
NGINX setup
```shell script
yum install nginx
mkdir sites-available
mkdir sites-enabled
```
Add site-enabled to `nginx.conf`

`include /etc/nginx/sites-enabled;`

Postgres setup
```shell script
yum install centos-release-scl-rh
yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum install postgresql12-server postgresql12-contrib postgres12-devel
sudo /usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl start postgresql-12
sudo systemctl enable  postgresql-12
```
START SERVICES
```shell script
pg_ctl -D /usr/local/var/postgres start
sudo systemctl restart redis-server.service
sudo systemctl enable rabbitmq-server
```
 
GETTING STARTED:
- Clone the repo `git clone https://bitbucket.org/swedishason/bridgero/`
- Create a virtual environment using the following command : `python3 -m venv /path/to/new/virtual/environment`
- Activate your virtual environment using `source (virtualenv name)/bin/activate`
- Create a new branch from the develop branch using the command `git checkout -b your_branch_name`
- Install project requirements depending on your environment using `pip install -r requirements.txt`
- Create `settings.ini` files to suit environments check `settings.ini.example` for your samples
- Edit the `.ini` files with your own credentials. Eg : database username, password ,etc., update the .gitignore file with the .env files
- Create a Postgres database profile that corresponds with what you have in the .env file.
- Set your environement variable by running the following command source .env
- Add a logs folder to the base directory of the project, update the .gitignore file with the logs folder.
- Run the command `python manage.py migrate` to create database tables.
- Run the command `python manage.py collectstatic --noinput` to load static files
- Create an admin: python manage.py createsuperuser.

Run server: python manage.py runserver 8000

Access Web: go to http://127.0.0.1:8000


from django.urls import path, include
from accounts.api.urls import router as accounts_router
# Routers for the various apps
# from core.api.urls import router as core_router
from utils.authentication import BasicAuthentication


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('accounts.api.urls')),

]

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi



schema_view = get_schema_view(
   openapi.Info(
      title="JitControl API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="info@jitcontrol.io"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
    authentication_classes=(BasicAuthentication ,),

)

urlpatterns += [
   url(r'^swagger/$', schema_view.with_ui(cache_timeout=0), name='schema-swagger-ui'),
]

from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
optional = {
    'blank': True,
    'null': True
}


class User(AbstractUser):
    """
    Base user model, can be extended to set properties for all users
    """
    company_name = models.CharField(max_length=1, **optional)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "User"

from rest_framework import serializers

from utils.serializers import BaseSerializer
from ..models import User


class UserSerializer(BaseSerializer):
    """
    Seriliazer for user class
    """
    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', ]

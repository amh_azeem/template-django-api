from django.urls import path, include
from rest_framework import routers

from accounts.api.views import UserViewSet

urlpatterns=[
    path('', include('rest_auth.urls')),
    path('registration', include('rest_auth.registration.urls')),
]

router = routers.SimpleRouter()
router.register(r'user', UserViewSet)

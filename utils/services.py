import json

def convert_to_json(data):
    """
    Helper function to ease conversion of json files
    :param data:
    :return:
    """
    data= {}
    try:
        val = byte_to_string(content)
        data = json.loads(val)
    except json.JSONDecodeError:
        encoding = 'ISO-8859-1'
        val = byte_to_string(content)
        data = json.loads(val,encoding=encoding)
    except :
        logger.error('CANNOT DECODE JSON')
    finally:
        return data



def byte_to_string(val, code='utf-8'):
    if isinstance(val, bytes):
        return val.decode(code)
    return val


class NotificationService:
    ACTIONS = ['UPDATE', 'SYNC', 'DELETE', 'CREATE', '']

    def fetch_info(self, obj, ):
        pass

    def generate_metadata(self, obj):
        """
        :param obj:
        :return: dict containing model,module and pk
        """

    def set_as_updated(self, obj, recipient, description):
        """
        Objects is a model, and by default, it has a pk, so we can pick leverage on that
        :param description: Message to be sent
        :param recipient: Who owns the notification
        :param obj: The object on which the notification was called for
        :return:
        """
        # notify.send(actor, recipient, verb, action_object, target, level, description, public, timestamp, **kwargs)
        # Notification.objects.create
        pass



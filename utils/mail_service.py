import smtplib
import threading

from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend
from django.core.mail.message import sanitize_address
from django.db.models import QuerySet

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
import logging

from utils.models import MessageLog

standard_logger = logging.getLogger(__name__)


class EmailThread(threading.Thread):
    def __init__(self, mass_mailer, is_single=False):
        threading.Thread.__init__(self)
        self.mailer = mass_mailer
        self.daemon = True
        self.is_single = is_single
        self.start()

    def run(self):
        if self.is_single:
            self.mailer.send_mail()
        else:
            self.mailer.trigger_mail()


class CustomMailAlternative(EmailMultiAlternatives):
    def __init__(self, *args, **kwargs):
        super().__init__(*args)
        self.message_obj = None
        if 'message_obj' in kwargs:
            self.message_obj = kwargs['message_obj']


class APPMailer:
    def __init__(self,
                 subject: str,
                 message: str,
                 to_address: str,
                 header_text: str = "",
                 ):
        self.message_item = MessageLog.objects.create(
            subject=subject,
            to_address=to_address,
            header_text=header_text,
            content=message,
        )

    def send_mail(self):
        EmailThread(self.message_item, is_single=True)


class APPMassMailer:
    def __init__(self, mail_list, is_queryset=False):
        if is_queryset:
            messages = mail_list
            self.total_messages = messages.count()
        else:
            messages = [mail.message_item for mail in mail_list]
            self.total_messages = len(messages)
        self.messages = list(map(generate_message, messages))

    def send(self):
        EmailThread(self)

    def trigger_mail(self):
        from django.core import mail
        connection = mail.get_connection()
        # Manually open the connection
        connection.open()
        # Send the all the  emails in a single call -
        count = connection.send_messages(self.messages, self.total_messages)
        # The connection was already open so send_messages() doesn't close it.
        # We need to manually close the connection.
        connection.close()


def generate_message(message):
    plaintext = get_template('utils/email_template.txt')
    htmly = get_template('utils/email_template.html')

    # Checks if host user is an email address
    if '.com' in settings.EMAIL_HOST_USER:
        formatted_sender_name = message.header_text + " <{}>".format(settings.EMAIL_HOST_USER)
    else:
        formatted_sender_name = message.header_text + '<{}>'.format(settings.SES_EMAIL)

    context = {
        'sender_name': message.header_text,
        'subject': message.subject,
        'message': message.content
    }

    subject, from_email, to = message.subject, formatted_sender_name, message.to_address
    text_content = plaintext.render(context)
    html_content = htmly.render(context)
    msg = CustomMailAlternative(subject, text_content, from_email, [to], message_obj=message)
    msg.attach_alternative(html_content, "text/html")
    return msg


def send_mail_with_client(message):
    message = generate_message(message)
    message.send()

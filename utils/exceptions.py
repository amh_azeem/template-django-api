from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException


class InvalidSubscription(APIException):
    status_code = 400
    default_detail = 'Invalid Supplier, kindly supply a valid supplier'
    default_code = 'service_unavailable'


class SubscriptionNotFound(APIException):
    status_code = 404
    default_detail = 'Subscription not found'
    default_code = 'not_found'



def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    detail = None
    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['code'] = response.status_code
        detail = response.data.pop('detail', None)
        if detail:
            if 'JSON parse' in detail:
                response.data['errors'] = "Bad JSON format"
            else:
                response.data['errors'] = detail

    return response

class AppBaseException(Exception):
    def __str__(self):
        length = len(self.args)
        if length == 0:
            return ''
        elif length == 1:
            return self.args[0]
        else:
            return self.args




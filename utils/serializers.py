from django.contrib.auth import get_user_model
from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import TokenSerializer
from rest_framework.utils.serializer_helpers import ReturnDict
from rest_framework_guardian.serializers import ObjectPermissionsAssignmentMixin

from .mixins import FriendlyErrorMessagesMixin
# from rest_framework_friendly_errors.mixins import FriendlyErrorMessagesMixin
from drf_queryfields import QueryFieldsMixin
from rest_framework import serializers
from utils.services import test_module_is_installed, check_credential_works_for_client


class PermaLinkedSerializer(serializers.ModelSerializer):
    """
    To add permalinks to models

    always ensure you add this to the inheriting model

    class Meta:
        fields = BasicSerializer.Meta.fields + ('additional_field',)
    """
    permalink = serializers.SerializerMethodField(read_only=True)

    def get_permalink(self, instance):
        """
        Returns fully qualified url to object
        :param instance:
        :return:
        """
        return self.context['request'].build_absolute_uri()


EXCLUDED_FIELDS = []


class BaseSerializer(ObjectPermissionsAssignmentMixin, FriendlyErrorMessagesMixin, QueryFieldsMixin,
                     serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = ['id']

    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        # readers = Group.objects.get(name='readers')
        # supervisors = Group.objects.get(name='supervisors')

        return {
            f'view_{self.Meta.model._meta.model_name.lower()}': [current_user],
            f'change_{self.Meta.model._meta.model_name.lower()}': [current_user],
            f'delete_{self.Meta.model._meta.model_name.lower()}': [current_user]
        }

    @property
    def data(self):
        ret = {'data':super().data}
        return ReturnDict(ret, serializer=self)


class CustomRegisterSerializer(RegisterSerializer):
    class Meta:
        model = get_user_model()
        fields = ['email', 'password1', 'password2']


class CustomSerializer(BaseSerializer):
    class Meta:
        exclude = EXCLUDED_FIELDS

    def validate_name(self, value):
        if test_module_is_installed(value):
            return value
        raise serializers.ValidationError(f'{value} cannot be used, kindly import the module from the libs server')


class CustomCredentialsSerializer(BaseSerializer):
    class Meta:
        exclude = EXCLUDED_FIELDS
        read_only_fields = ('user',)

    def validate(self, data):
        """
        We check for errors,
        :param data:
        :return: data if it passes
        """
        supplier = data['supplier']
        actual_values = data['data']
        check_credential_works_for_client(actual_values, supplier, serializers.ValidationError)
        return data

    def create(self, validated_data):
        # Overiding the save method to autho attach user as owner of the object, we can overide this with a force insert
        validated_data['user'] = self.context['request'].user
        return super(CustomCredentialsSerializer, self).create(validated_data)



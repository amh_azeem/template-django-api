import enum
import logging

from django.contrib.contenttypes.models import ContentType

from django.db import models

from django.urls import reverse
from django.utils import timezone


optional = {
    'blank': True,
    'null': True
}


class ExtendedQuerySet(models.QuerySet):
    """
    We override the models to not delete records,
    """

    def delete(self):
        return super(ExtendedQuerySet, self).update(
            is_deleted=True, deleted_on=timezone.now()
        )

    def hard_delete(self):
        return super(ExtendedQuerySet, self).delete()

    def not_deleted(self):
        return self.filter(is_deleted=False)

    def deleted(self):
        return self.filter(is_deleted=True)


class ExtendedModelManager(models.Manager):
    """
    This manager would require you to query items via available() method rather than all()
    """

    def get_queryset(self):
        return ExtendedQuerySet(self.model, using=self._db)

    def available(self):
        return self.get_queryset().not_deleted().order_by('pk')

    def deleted(self):
        return self.get_queryset().deleted().order_by('pk')

    def hard_delete(self):
        return self.get_queryset().hard_delete()

    def delete(self):
        return self.get_queryset().delete()




class BaseModel(models.Model):
    """
    Simple base class used to append created on and modified on to all models that inherits it
    """
    # is_deleted = models.BooleanField(default=False, editable=False)
    # deleted_on = models.DateTimeField(**optional, editable=False)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    updated_on = models.DateTimeField(auto_now=True, editable=False)

    # objects = ExtendedModelManager

    def hard_delete(self):
        return super(BaseModel, self).delete()

    # def delete(self, *args, **kwargs):
    #     """
    #     Overide the delete method to only change its is_deleted field to true
    #     :param args:
    #     :param kwargs:
    #     :return:
    #     """
    #     self.is_deleted = True
    #     self.deleted_on = timezone.now()
    #     self.save()
    #     return (self.pk, self)

    def get_absolute_url(self):
        return f"{self.pk}"

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    class Meta:
        abstract = True



class Status(enum.Enum):
    Updated = "Updated"
    Sync = "Sync"
    Deleted = "Deleted"
    Created = "Created"


class MessageLog(SoftDeleteModel):
    subject = models.CharField(max_length=255)
    to_address = models.CharField(max_length=255)
    content = models.CharField(max_length=255)
    has_sent = models.BooleanField(default=True)

    class Meta:
        abstract = True

    def set_as_sent(self):
        self.has_sent = True
        self.error_message = ""
        self.save()

    def set_as_fail(self, error=""):
        self.error_message = error
        self.has_sent = False
        self.save()


    def send_mail(self):
        from .mail_service import send_mail_with_client
        try:
            send_mail_with_client(self)
            self.set_as_sent()
        except Exception as error:
            self.set_as_fail(error.__str__())
